/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp_nn;

import java.util.ArrayList;

/**
 *
 * @author Angius
 */
public class City {
    private int xCoord;
    private int yCoord;
    private String name;
    
    // Main city object
    public City(String name, int xCoord, int yCoord)
    {
        this.name = name;
        this.xCoord = xCoord;
        this.yCoord = yCoord;
    }
    
    // Return city name
    public String GetName() { return name; }
   
    // Return city X coordinate
    public int Xcoord() { return xCoord; }
    
    // Return city Y coordinate
    public int Ycoord() { return yCoord; }
    
    // Return distance between cities
    public double Distance(City city)
    {
        int deltaX = city.Xcoord() - this.Xcoord();
        int deltaY = city.Ycoord() - this.Ycoord();
        
        double bracketA = Math.pow(deltaX, 2);
        double bracketB = Math.pow(deltaY, 2);
        
        double result = Math.sqrt(bracketA + bracketB);
        
        return result;
    }
    
    // Overload toString()
    public String toString() { return this.name; }   
}

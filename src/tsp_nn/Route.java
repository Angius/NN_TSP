/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp_nn;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Angius
 */
public class Route {
    private ArrayList<City> cities = new ArrayList<>();
    
    // Main route object
    public Route(ArrayList<City> cities) { this.cities.addAll(cities); }
    
    // Class to get cities
    public ArrayList<City> getCities() { return cities; }
    
    // Class to calculate total distance
    public double calculateTotalDistance()
    {
       int citiesSize = this.getCities().size();
       
       return (double) (this.getCities().stream().mapToDouble(x -> {
           int cityIndex = this.getCities().indexOf(x);
           double returnValue = 0;
           if (cityIndex < citiesSize - 1) returnValue = x.Distance(this.getCities().get(cityIndex + 1));
           return returnValue;
       }).sum() + this.getCities().get(citiesSize - 1).Distance(this.getCities().get(0)));
    }
    
    // Overload toString
    public String toString() { return Arrays.toString(cities.toArray()); }
    
}

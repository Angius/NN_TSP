/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp_nn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
/**
 *
 * @author Angius
 */
public class TSP_NN {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        TSP_NN driver = new TSP_NN();
        
        ArrayList<City> cities = new ArrayList<>();
        
        System.out.println("---------------------------------------------------------------");
        System.out.println("Input your cities in a NAME,X,Y format");
        System.out.println("Press enter without any input to calculate");
        System.out.println("---------------------------------------------------------------");
        
        // Take user input
        boolean inputControl = true;
        while (inputControl)
        {
            String inputString = br.readLine();
            
            if (!inputString.isEmpty())
            {
                String[] inputArray = inputString.split(",");

                City inputCity = new City(inputArray[0], Integer.parseInt(inputArray[1]), Integer.parseInt(inputArray[2]));

                cities.add(inputCity);
            }
            else
            {
                inputControl = false;
            }
        }

        Route shortestRoute = new NearestNeighbor().findShortestRoute(cities);
        
        driver.printShortestRoute(shortestRoute);
        driver.printShortestRouteWithReturn(shortestRoute);
        
    }
    
    private void printShortestRoute(Route shortestRoute)
    {
        System.out.println("---------------------------------------------------------------");
        System.out.println("Shortest route so far       ==> " + shortestRoute);
        System.out.println("With total distance:            " + shortestRoute.calculateTotalDistance());
        System.out.println("---------------------------------------------------------------");
    }
    
    private void printShortestRouteWithReturn(Route shortestRoute)
    {
        ArrayList<City> completeCities = new ArrayList<>();
        
        // Print new complete route                        "
        System.out.print("Shortest route with return  ==> [");
        for(int i = 0; i < shortestRoute.getCities().size(); i++)
        {
            System.out.print(shortestRoute.getCities().get(i).toString() + ", ");
            completeCities.add(shortestRoute.getCities().get(i));
        }
        System.out.println(shortestRoute.getCities().get(0) + "]");
        completeCities.add(shortestRoute.getCities().get(0));
        
        // Create complete route array
        Route completeRoute = new Route(completeCities);
            
        // Print complete route distance
        System.out.println("With total distance:            " + completeRoute.calculateTotalDistance());
        System.out.println("---------------------------------------------------------------");
    }
}

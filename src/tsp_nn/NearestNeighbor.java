/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp_nn;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Angius
 */
public class NearestNeighbor {
    public Route findShortestRoute(ArrayList<City> cities)
    {
        ArrayList<City> shortestRouteCities = new ArrayList<>(cities.size());
        
        // Print some stuff
        System.out.println("---------------------------------------------------------------");
        System.out.println("Initial route        ==> " + Arrays.toString(cities.toArray()));
        System.out.println("With total distance:     " + new Route(cities).calculateTotalDistance());
        System.out.println("---------------------------------------------------------------");
        
        // Pick random city
        City city = cities.get(0); //(int)(cities.size() * Math.random()));
        
        // Update routes
        updateRoutes(shortestRouteCities, cities, city);
        
        while (cities.size() >= 1)
        {
            city = getNextCity(cities, city);
            updateRoutes(shortestRouteCities, cities, city);
        }
        
        return new Route(shortestRouteCities);
    }
    
    // Method to update routes
    private void updateRoutes(ArrayList<City> shortestRouteCities, ArrayList<City> cities, City city)
    {
        shortestRouteCities.add(city);
        cities.remove(city);
        
        System.out.println("Cities in shortest route ==> " + Arrays.toString(shortestRouteCities.toArray()));
        System.out.println("Remaining cities         ==> " + Arrays.toString(cities.toArray()) + "\n");
    }
    
    // Method to get the next city
    private City getNextCity(ArrayList<City> cities, City city)
    {
        return cities.stream().min((city1, city2) -> {
            int flag = 0;
            if (city1.Distance(city) < city2.Distance(city)) flag = -1;
            else if (city1.Distance(city) > city2.Distance(city)) flag = 1;
            return flag;
        }).get();
    }
}
